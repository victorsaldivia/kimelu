import React, {Component} from 'react';
import {Text, View} from 'react-native';
import firebaseConfig from "./application/utils/firebase";
import * as firebase from "firebase";
firebase.initializeApp(firebaseConfig);
import GuestNavigation from "./application/navigations/guest";

export default class App extends Component {

  render(){
    return (
          <GuestNavigation/>
      
    );
  }
}
