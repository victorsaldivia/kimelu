import React from "react";
import { View, ActivityIndicator, StyleSheet, Dimensions} from 'react-native';


const {height} = Dimensions.get('window');
export default class PreLoader extends Component{
    render(){
        return(
            <View style = {styles.preLoader}>
                <ActivityIndictor style = {{height = 80}} size = "large" color="#D84339"/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    preLoader: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      height: 'height',
    },
  });
  
  