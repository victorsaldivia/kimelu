import React from 'react';
import {createAppContainer, createSwitchNavigator} from "react-navigation";
import Start from "../screens/Start";
import Login from "../screens/Login";


export default createAppContainer(createSwitchNavigator(
    {Start, Login},
    {initialRouteName: 'Start'}
));