import React from "react";
import { StyleSheet, StatusBar } from "react-native";
import { Font } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text } from 'native-base';


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }
  
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <Container>
        <Header style={styles.header} />
        <Content />
        <Footer style={styles.footer}>
          <FooterTab style={styles.footertab}>
            <Button vertical>
              <Icon name="home" style={{ color: "#ffffff" }}/>
              <Text style={styles.textContent}>Inicio</Text>
            </Button>
            <Button vertical>
              <Icon name="card" style={{ color: "#ffffff" }} />
              <Text style={styles.textContent}>Ticket</Text>
            </Button>
            <Button vertical active>
              <Icon active name="cog" style={{ color: "#ffffff" }} />
              <Text style={styles.textContent}>Ajustes</Text>
            </Button>
            <Button vertical>
              <Icon name="person" style={{ color: "#ffffff" }} />
              <Text style={styles.textContent}>Contacto</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#D84339',
  },
  footertab: {
    flex: 1,
    backgroundColor: '#D84339',
  },
  footer: {
    backgroundColor: '#D84339',
  },
  textContent:{
    color: '#ffffff',
  }
});
