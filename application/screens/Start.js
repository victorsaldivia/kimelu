import React, {Component} from 'react';
import { View } from 'react-native';
import BackgroundImage from '../components/BackgroundImage';
import Btn from '../components/Btn';
import { NavigationActions } from 'react-navigation';
import Toast from 'react-native-simple-toast';
import * as firebase from 'firebase';

export default class Start extends Component{   

    login (){
        const navigateAction = NavigationActions.navigate({
            routeName: "Login"
        })
        this.props.navigation.dispatch(navigateAction);
    }
    register (){
        
    }
    
    render (){
       return(
            <BackgroundImage source = {require ('../../assets/images/login-bg0.png')}>
                <View style = {{justifyContent: 'flex-end', flex: 1}}>
                    <Btn
                        bgColor="#0374A1"
                        title="Entrar  "
                        action={this.login.bind(this)}
                        iconName="sign-in"
                        iconSize={35}
                        iconColor="#FFFFFF"
                    />
                    <Btn
                        bgColor="#098405"
                        title="Registrarse  "
                        action={this.register.bind(this)}
                        iconName="user-plus"
                        iconSize={35}
                        iconColor="#FFFFFF"
                    />
                </View>
            </BackgroundImage> 
       );
    }
}
